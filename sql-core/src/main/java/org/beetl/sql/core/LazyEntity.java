package org.beetl.sql.core;

/**
 * @author xiandafu
 */
public interface LazyEntity extends java.io.Serializable {
  Object get();

}
